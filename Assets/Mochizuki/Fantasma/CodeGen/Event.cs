﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;
using Mochizuki.Fantasma.Extensions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Event : IMemberDeclaration<EventDeclarationSyntax>
    {
        private readonly EventInfo _event;

        public Type EventHandlerType => _event.EventHandlerType;

        public Event(EventInfo @event)
        {
            _event = @event;
        }

        public EventDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
            var @event = SyntaxFactory.EventDeclaration(SyntaxFactory.ParseTypeName(EventHandlerType.NormalizedName()), _event.Name).AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray());

            return @event;
        }

        public ReadOnlyCollection<Type> References => new ReadOnlyCollection<Type>(new List<Type> { EventHandlerType });
    }
}