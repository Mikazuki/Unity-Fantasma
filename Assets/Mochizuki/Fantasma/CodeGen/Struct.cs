﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Abstractions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Struct : TypeDeclaration<StructDeclarationSyntax>
    {
        private readonly List<Type> _baseClasses;
        private readonly List<Type> _references;
        private readonly Type _type;

        public Struct(Type type) : base(type)
        {
            _type = type;
            _baseClasses = new List<Type>();
            _references = new List<Type>();
        }

        public override StructDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
            var @struct = SyntaxFactory.StructDeclaration(_type.Name).AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray());

            return @struct;
        }
    }
}