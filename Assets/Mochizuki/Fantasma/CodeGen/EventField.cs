﻿using System;
using System.Collections.ObjectModel;

using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class EventField : IMemberDeclaration<EventFieldDeclarationSyntax>
    {
        public EventFieldDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Type> References { get; }
    }
}