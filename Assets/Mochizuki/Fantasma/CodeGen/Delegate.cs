﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;
using Mochizuki.Fantasma.Extensions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Delegate : IMemberDeclaration<DelegateDeclarationSyntax>
    {
        private readonly MethodInfo _delegate;
        private readonly Type _type;

        public Type ReturnType => _delegate.ReturnType;
        public ReadOnlyCollection<Type> ArgumentTypes => _delegate.GetParameters().Select(w => w.ParameterType).ToList().AsReadOnly();
        public bool IsIsolatedDelegate => _delegate.DeclaringType == null;

        public Delegate(Type type)
        {
            _type = type;

            _delegate = type.GetMethod("Invoke");
            if (_delegate == null)
                throw new InvalidOperationException(); // @delegate is not delegate
        }

        public DelegateDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
            var identifier = _type.IsGenericType ? _type.Name.Substring(0, _type.Name.IndexOf('`')) : _type.Name;
            var @delegate = SyntaxFactory.DelegateDeclaration(SyntaxFactory.ParseTypeName(ReturnType.NormalizedName()), identifier).AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray());

            if (ArgumentTypes.Count == 0)
                return @delegate;

            var parameters = new List<ParameterSyntax>();
            foreach (var param in _delegate.GetParameters())
            {
                var parameter = SyntaxFactory.Parameter(SyntaxFactory.Identifier(param.Name))
                                             .WithType(SyntaxFactory.ParseTypeName(param.ParameterType.NormalizedName()));
                if (param.IsOut)
                {
                    parameter = parameter.WithType(SyntaxFactory.ParseTypeName(param.ParameterType.GetElementType().NormalizedName()));
                    parameter = parameter.AddModifiers(SyntaxFactory.Token(SyntaxKind.OutKeyword));
                }

                parameters.Add(parameter);
            }

            @delegate = @delegate.AddParameterListParameters(parameters.ToArray());

            if (_delegate.GetParameters().All(w => !w.ParameterType.IsGenericParameter()))
                return @delegate;

            var generics = _delegate.GetParameters()
                                    .Where(w => w.ParameterType.IsGenericParameter())
                                    .Select(w => w.ParameterType.IsGenericParameter ? w.ParameterType.NormalizedName() : w.ParameterType.GetElementType().NormalizedName())
                                    .Select(SyntaxFactory.TypeParameter);

            return @delegate.AddTypeParameterListParameters(generics.ToArray());
        }

        public ReadOnlyCollection<Type> References => ArgumentTypes.Concat(new List<Type> { ReturnType }).ToList().AsReadOnly();
    }
}