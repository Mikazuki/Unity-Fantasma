﻿using System;
using System.Collections.ObjectModel;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class EnumMember : IMemberDeclaration<EnumMemberDeclarationSyntax>
    {
        private readonly string _name;
        private readonly object _value;

        public EnumMember(string name, object value)
        {
            _name = name;
            _value = value;
        }

        public EnumMemberDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            return SyntaxFactory.EnumMemberDeclaration(_name).WithEqualsValue(SyntaxFactory.EqualsValueClause(SyntaxFactory.ParseExpression(Convert.ToInt64(_value).ToString())));
        }

        public ReadOnlyCollection<Type> References => throw new NotImplementedException();
    }
}