﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Abstractions;
using Mochizuki.Fantasma.Extensions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Interface : TypeDeclaration<InterfaceDeclarationSyntax>
    {
        private readonly Type _type;

        public Interface(Type type) : base(type)
        {
            _type = type;
        }

        public override InterfaceDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
            var @interface = SyntaxFactory.InterfaceDeclaration(_type.Name).AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray());

            if (Implementations.Count > 0)
                @interface = @interface.AddBaseListTypes(Implementations.Select(w => SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName(w.NormalizedName()))).Cast<BaseTypeSyntax>().ToArray());

            return @interface.AddMembers(MemberDeclarations.Select(w => w.DeclarationToSyntax(false)).ToArray());
        }
    }
}