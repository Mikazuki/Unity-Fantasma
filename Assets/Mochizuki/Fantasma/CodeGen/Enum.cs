﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;
using Mochizuki.Fantasma.Extensions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Enum : ITypeDeclaration<EnumDeclarationSyntax>
    {
        private readonly List<Type> _baseClasses;
        private readonly List<EnumMember> _members;
        private readonly Type _type;

        public Enum(Type type)
        {
            _type = type;

            _baseClasses = new List<Type>();
            _members = new List<EnumMember>();
        }

        public bool HasNestedClass => _type.GetNestedTypes().Length > 0;
        public ReadOnlyCollection<Type> NestedClasses => _type.GetNestedTypes().Where(w => !w.IsDelegate()).ToList().AsReadOnly();
        public ReadOnlyCollection<Type> References => new ReadOnlyCollection<Type>(new List<Type>()); // always empty
        public ReadOnlyCollection<Type> AliasReferences => new ReadOnlyCollection<Type>(new List<Type>()); // always empty

        public EnumDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
            var @enum = SyntaxFactory.EnumDeclaration(_type.Name).AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray());

            if (_baseClasses.Count > 0)
            {
                var @base = _baseClasses.Select(w => SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName(w.NormalizedName()))).Cast<BaseTypeSyntax>().ToArray();
                @enum = @enum.AddBaseListTypes(@base);
            }

            return @enum.AddMembers(_members.Select(w => w.DeclarationToSyntax(true)).ToArray());
        }

        public void AddDeclarations(params ITypeDeclaration<BaseTypeDeclarationSyntax>[] declarations) { }

        public void Extract()
        {
            if (System.Enum.GetUnderlyingType(_type) != typeof(int))
                _baseClasses.Add(System.Enum.GetUnderlyingType(_type));

            foreach (var name in System.Enum.GetNames(_type))
                _members.Add(new EnumMember(name, System.Enum.Parse(_type, name)));
        }
    }
}