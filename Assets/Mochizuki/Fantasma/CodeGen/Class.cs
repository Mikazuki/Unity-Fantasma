﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Abstractions;
using Mochizuki.Fantasma.Extensions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Class : TypeDeclaration<ClassDeclarationSyntax>
    {
        private readonly Type _type;

        public Class(Type type) : base(type)
        {
            _type = type;
        }

        public override ClassDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
            if (_type.IsAbstract && _type.IsSealed)
                modifiers.Add(SyntaxKind.StaticKeyword);
            else if (_type.IsAbstract)
                modifiers.Add(SyntaxKind.AbstractKeyword);
            else if (_type.IsSealed)
                modifiers.Add(SyntaxKind.SealedKeyword);

            var @class = SyntaxFactory.ClassDeclaration(_type.Name).AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray());

            if (Implementations.Count > 0)
                @class = @class.AddBaseListTypes(Implementations.Select(w => SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName(w.NormalizedName()))).Cast<BaseTypeSyntax>().ToArray());

            @class = @class.AddMembers(MemberDeclarations.Select(w => w.DeclarationToSyntax(true)).ToArray());

            if (NestedDeclarations.Count > 0)
                @class = @class.AddMembers(NestedDeclarations.Select(w => w.DeclarationToSyntax(true)).Cast<MemberDeclarationSyntax>().ToArray());

            return @class;
        }
    }
}