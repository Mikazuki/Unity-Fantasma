﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;
using Mochizuki.Fantasma.Extensions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class MethodInterface : IMemberDeclaration<MethodDeclarationSyntax>
    {
        private readonly MethodInfo _method;

        public Type ReturnType => _method.ReturnType;

        public IReadOnlyList<Type> ArgumentTypes => _method.GetParameters().Select(w => w.ParameterType).ToList().AsReadOnly();

        public MethodInterface(MethodInfo method)
        {
            _method = method;
        }

        public MethodDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var method = SyntaxFactory.MethodDeclaration(SyntaxFactory.ParseTypeName(ReturnType.NormalizedName()), _method.Name);

            if (implementation)
            {
                var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
                if (_method.IsStatic)
                    modifiers.Add(SyntaxKind.StaticKeyword);
                if (_method.IsAbstract)
                    modifiers.Add(SyntaxKind.AbstractKeyword);
                else if (_method.IsOverride())
                    modifiers.Add(SyntaxKind.OverrideKeyword);
                else if (_method.IsVirtual)
                    modifiers.Add(SyntaxKind.VirtualKeyword);

                var statements = new List<StatementSyntax>();
                if (ReturnType != typeof(void))
                    statements.Add(SyntaxFactory.ParseStatement("return default;"));

                method = method.AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray())
                               .WithBody(SyntaxFactory.Block(statements));
            }
            else
            {
                method = method.WithSemicolonToken(SyntaxFactory.Token(SyntaxKind.SemicolonToken));
            }

            if (ArgumentTypes.Count == 0)
                return method;

            var parameters = new List<ParameterSyntax>();

            foreach (var param in _method.GetParameters())
            {
                var parameter = SyntaxFactory.Parameter(SyntaxFactory.Identifier(param.Name))
                                             .WithType(SyntaxFactory.ParseTypeName(param.ParameterType.NormalizedName()));
                if (param.IsOut)
                {
                    // see: https://stackoverflow.com/questions/18787589/why-is-isgenericparameter-false-for-a-generic-parameter-out-t
                    parameter = parameter.WithType(SyntaxFactory.ParseTypeName(param.ParameterType.GetElementType().NormalizedName()));
                    parameter = parameter.AddModifiers(SyntaxFactory.Token(SyntaxKind.OutKeyword));
                }

                parameters.Add(parameter);
            }

            method = method.AddParameterListParameters(parameters.ToArray());

            if (_method.GetParameters().All(w => !w.ParameterType.IsGenericParameter()))
                return method;

            var generics = _method.GetParameters()
                                  .Where(w => w.ParameterType.IsGenericParameter())
                                  .Select(w => w.ParameterType.IsGenericParameter ? w.ParameterType.NormalizedName() : w.ParameterType.GetElementType().NormalizedName())
                                  .Select(SyntaxFactory.TypeParameter);

            return method.AddTypeParameterListParameters(generics.ToArray());
        }

        public ReadOnlyCollection<Type> References => ArgumentTypes.Concat(new List<Type> { ReturnType }).ToList().AsReadOnly();
    }
}