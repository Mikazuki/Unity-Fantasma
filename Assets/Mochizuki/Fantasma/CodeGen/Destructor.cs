﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Destructor : IMemberDeclaration<DestructorDeclarationSyntax>
    {
        public DestructorDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Type> References => new ReadOnlyCollection<Type>(Array.Empty<Type>().ToList());
    }
}