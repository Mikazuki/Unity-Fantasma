﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;
using Mochizuki.Fantasma.Extensions;

namespace Mochizuki.Fantasma.CodeGen
{
    internal class Field : IMemberDeclaration<FieldDeclarationSyntax>
    {
        private readonly FieldInfo _field;

        public Type Type => _field.FieldType;

        public string Name => _field.Name;

        public Field(FieldInfo field)
        {
            _field = field;
        }

        public FieldDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            var variable = SyntaxFactory.VariableDeclaration(CreateFieldTypeSyntax()).AddVariables(SyntaxFactory.VariableDeclarator(Name));
            var modifiers = new List<SyntaxKind> { SyntaxKind.PublicKeyword };
            if (_field.IsStatic && !_field.IsLiteral)
                modifiers.Add(SyntaxKind.StaticKeyword);
            if (_field.IsInitOnly)
                modifiers.Add(SyntaxKind.ReadOnlyKeyword);
            if (_field.IsLiteral)
                modifiers.Add(SyntaxKind.ConstKeyword);

            return SyntaxFactory.FieldDeclaration(variable).AddModifiers(modifiers.Select(SyntaxFactory.Token).ToArray());
        }

        public ReadOnlyCollection<Type> References => new ReadOnlyCollection<Type>(new List<Type> { Type });

        private TypeSyntax CreateFieldTypeSyntax()
        {
            return SyntaxFactory.ParseTypeName(Type.NormalizedName().Replace($"{_field.DeclaringType.Namespace}.", "").Replace("+", "."));

            /*
            if (_field.DeclaringType?.DeclaringType == null || string.IsNullOrWhiteSpace(Type.FullName) || string.IsNullOrWhiteSpace(Type.Namespace))
            {
                if (Type.DeclaringType == null || string.IsNullOrWhiteSpace(Type.FullName) || string.IsNullOrWhiteSpace(Type.Namespace))
                    return SyntaxFactory.ParseTypeName(Type.NormalizedName());

                if (string.IsNullOrWhiteSpace(_field.DeclaringType?.Namespace))
                    return SyntaxFactory.ParseTypeName(Type.FullName.Replace("+", ""));
                return SyntaxFactory.ParseTypeName(Type.NormalizedFullName().Replace($"{_field.DeclaringType.Namespace}.", "").Replace("+", "."));
            }

            return SyntaxFactory.ParseTypeName(Type.FullName.Replace($"{_field.DeclaringType.Namespace}.", "").Replace("+", "."));
            */
        }
    }
}