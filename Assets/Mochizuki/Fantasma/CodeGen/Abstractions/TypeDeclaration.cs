﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Interfaces;
using Mochizuki.Fantasma.Extensions;
using Mochizuki.Fantasma.Internal;

namespace Mochizuki.Fantasma.CodeGen.Abstractions
{
    internal abstract class TypeDeclaration<T> : ITypeDeclaration<T> where T : BaseTypeDeclarationSyntax
    {
        private const BindingFlags FLAGS = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
        private readonly List<Type> _aliasReferences;
        private readonly List<Type> _implementations;
        private readonly List<IMemberDeclaration<MemberDeclarationSyntax>> _members;
        private readonly List<ITypeDeclaration<BaseTypeDeclarationSyntax>> _nestedDeclarations;
        private readonly List<Type> _references;
        private readonly Type _type;

        protected IReadOnlyCollection<Type> Implementations => _implementations.AsReadOnly();

        protected IReadOnlyCollection<ITypeDeclaration<BaseTypeDeclarationSyntax>> NestedDeclarations => _nestedDeclarations.AsReadOnly();

        protected IReadOnlyCollection<IMemberDeclaration<MemberDeclarationSyntax>> MemberDeclarations => _members.AsReadOnly();

        protected TypeDeclaration(Type type)
        {
            _type = type;

            _aliasReferences = new List<Type>();
            _implementations = new List<Type>();
            _members = new List<IMemberDeclaration<MemberDeclarationSyntax>>();
            _nestedDeclarations = new List<ITypeDeclaration<BaseTypeDeclarationSyntax>>();
            _references = new List<Type>();
        }

        public abstract T DeclarationToSyntax(bool implementation);

        public bool HasNestedClass => _type.GetNestedTypes().Length > 0;
        public ReadOnlyCollection<Type> NestedClasses => _type.GetNestedTypes().Where(w => !w.IsDelegate()).ToList().AsReadOnly();
        public ReadOnlyCollection<Type> References => _references.AsReadOnly();
        public ReadOnlyCollection<Type> AliasReferences => _aliasReferences.AsReadOnly();

        public void AddDeclarations(params ITypeDeclaration<BaseTypeDeclarationSyntax>[] declarations)
        {
            foreach (var declaration in declarations)
            {
                foreach (var reference in declaration.References)
                    AddTypeReferencesIfNotExists(reference);

                _nestedDeclarations.Add(declaration);
            }
        }

        public virtual void Extract()
        {
            if (_type.BaseType != null && _type.BaseType != typeof(object))
            {
                _implementations.Add(_type.BaseType);
                AddTypeReferencesIfNotExists(_type.BaseType);
            }

            if (_type.GetDirectImplementedInterfaces().Length > 0)
                foreach (var @interface in _type.GetDirectImplementedInterfaces())
                {
                    _implementations.Add(@interface);
                    AddTypeReferencesIfNotExists(@interface);
                }

            // default constructor for code generation

            foreach (var member in _type.GetMembers(FLAGS).Select(CreateAbstract).Where(w => w != null).OrderBy(w => w, new MemberDeclarationComparer()))
            {
                _members.Add(member);
                foreach (var reference in member.References)
                    AddTypeReferencesIfNotExists(reference);
            }

            if (!_members.Where(w => w is Constructor).Cast<Constructor>().Any(w => w.IsDefaultConstructor))
            {
                var constructor = _members.FirstOrDefault(w => w is Constructor);
                if (constructor != null)
                    _members.Add(((Constructor) constructor).CreateDefaultConstructor());
            }
        }

        private static IMemberDeclaration<MemberDeclarationSyntax> CreateAbstract(MemberInfo member)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Constructor:
                    return new Constructor((ConstructorInfo) member, false);

                case MemberTypes.Event:
                    return new Event((EventInfo) member);

                case MemberTypes.Field:
                    return new Field((FieldInfo) member);

                case MemberTypes.Method when !((MethodInfo) member).IsSpecialName:
                    return new MethodInterface((MethodInfo) member);

                case MemberTypes.NestedType when ((Type) member).IsDelegate():
                    return new Delegate((Type) member);

                case MemberTypes.Property:
                    return new Property((PropertyInfo) member);

                default:
                    return null;
            }
        }

        protected void AddTypeReferencesIfNotExists(Type type)
        {
            if (type == null)
                return;

            var references = new List<Type>();

            void RecursiveExtractType(Type t)
            {
                if (t.IsGenericType)
                    t.GenericTypeArguments.ToList().ForEach(RecursiveExtractType);

                if (t.IsKeywordType())
                    return;

                references.Add(t);
            }

            RecursiveExtractType(type);

            foreach (var t in references)
            {
                if (_references.Any(w => w.FullName == t.FullName) || t.Namespace == _type.Namespace)
                    continue;
                _references.Add(t);
            }
        }
    }
}