﻿using System;

using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen.Abstractions;

using NUnit.Framework;

namespace Mochizuki.Fantasma.Tests.CodeGen.Abstractions
{
    [TestFixture]
    internal class TypeDeclarationTest : TypeDeclaration<ClassDeclarationSyntax>
    {
        public class AClass { }

        public TypeDeclarationTest(Type type) : base(type) { }

        public override ClassDeclarationSyntax DeclarationToSyntax(bool implementation)
        {
            throw new NotImplementedException();
        }

        [Test]
        [TestCase(typeof(AClass))]
        public void ExtractTest(Type t)
        {
            var instance = new TypeDeclarationTest(t);
            instance.Extract();
        }
    }
}