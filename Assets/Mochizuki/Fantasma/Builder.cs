﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Mochizuki.Fantasma.CodeGen;
using Mochizuki.Fantasma.CodeGen.Interfaces;
using Mochizuki.Fantasma.Extensions;
using Mochizuki.Fantasma.Internal;

using UnityEditor;

using Delegate = Mochizuki.Fantasma.CodeGen.Delegate;
using Enum = Mochizuki.Fantasma.CodeGen.Enum;

namespace Mochizuki.Fantasma
{
    public class Builder : ICodeGen<CompilationUnitSyntax>
    {
        private readonly HashSet<Assembly> _assemblies;
        private readonly DefaultAsset _asset;
        private readonly string _directive;

        private readonly ReadOnlyCollection<Regex> _dnt = new List<Regex>
        {
            new Regex("^UnityEngine.*$", RegexOptions.Compiled),
            new Regex("^UnityEditor.*$", RegexOptions.Compiled),
            new Regex("^mscorlib$", RegexOptions.Compiled),
            new Regex("^System.*$", RegexOptions.Compiled)
        }.AsReadOnly();

        private readonly DefaultAsset _root;
        private readonly List<CompilationUnit> _units;
        private List<Assembly> _reversed;

        public Builder(DefaultAsset asset, DefaultAsset root, string directive)
        {
            _asset = asset;
            _root = root;
            _directive = directive;

            _assemblies = new HashSet<Assembly>();
            _units = new List<CompilationUnit>();
        }

        public CompilationUnitSyntax DeclarationToSyntax(bool implementation)
        {
            throw new NotImplementedException();
        }

        public void Build()
        {
            var assembly = Assembly.LoadFrom(AssetDatabase.GetAssetPath(_asset));
            _assemblies.Add(assembly);

            FindDependencies(assembly);

            _reversed = _assemblies.Reverse().ToList();

            _units.AddRange(CreateClassCompilationUnits(_assemblies.ToList(), _directive));

            // NOTE: I have no way of knowing if a delegate defined outside of the class should write to the same file as the class.
            //       Therefore, it is write as a single class file with a special file prefix "FantasmaDelegate__${DELEGATE_NAME}.cs"
            //       Structurally, they should belong to a specific namespace (contains empty namespace), so as long as they are in the same namespace, they should be fine.
            _units.AddRange(CreateIndependentDelegateCompilationUnits(_assemblies.ToList(), _directive));

            Flush();
        }

        private void FindDependencies(Assembly assembly)
        {
            var assemblies = assembly.GetTypes()[0].Assembly.GetReferencedAssemblies();
            if (assemblies.Length == 0)
                return;

            foreach (var name in assemblies)
            {
                if (_dnt.Any(w => w.IsMatch(name.Name)))
                    continue;

                var asm = Assembly.Load(name);
                if (_assemblies.Any(w => w.FullName == asm.FullName))
                    continue;

                _assemblies.Add(asm);
                FindDependencies(asm);
            }
        }

        private static IEnumerable<CompilationUnit> CreateClassCompilationUnits(IEnumerable<Assembly> assemblies, string directive)
        {
            var classes = new List<KeyValuePair<Type, ITypeDeclaration<BaseTypeDeclarationSyntax>>>();
            var tuples = assemblies.SelectMany(w => w.GetTypes()).Where(w => (w.IsPublic || w.IsNestedPublic) && !w.IsDelegate()).Select(w => (item1: w, item2: CreateAbstractClass(w))).ToList();
            tuples.ForEach(w => w.item2.Extract());

            foreach (var (t, c) in tuples.Where(w => !w.item2.HasNestedClass))
                classes.Add(new KeyValuePair<Type, ITypeDeclaration<BaseTypeDeclarationSyntax>>(t, c));

            var remaining = tuples.Where(w => w.item2.HasNestedClass).ToList();
            while (remaining.Count > 0)
            {
                var removes = new List<Type>();
                foreach (var (t, c) in remaining)
                {
                    if (!c.NestedClasses.All(w => classes.Any(v => v.Key == w)))
                        continue;

                    var declarations = classes.Where(w => c.NestedClasses.Contains(w.Key)).ToList();
                    c.AddDeclarations(declarations.Select(w => w.Value).ToArray());

                    removes.Add(t);
                    classes.RemoveAll(w => declarations.Select(v => v.Key).Any(v => v == w.Key));
                    classes.Add(new KeyValuePair<Type, ITypeDeclaration<BaseTypeDeclarationSyntax>>(t, c));
                }

                foreach (var t in removes)
                    remaining.RemoveAt(remaining.FindIndex(w => w.item1 == t));
            }

            return classes.Select(w => WrapAsCompilationUnit(w.Key, w.Value, directive)).ToList();
        }

        private static IEnumerable<CompilationUnit> CreateIndependentDelegateCompilationUnits(IEnumerable<Assembly> assemblies, string directive)
        {
            var delegates = new List<KeyValuePair<Type, IMemberDeclaration<DelegateDeclarationSyntax>>>();
            var tuples = assemblies.SelectMany(w => w.GetTypes()).Where(w => (w.IsPublic || w.IsNestedPublic) && w.IsDelegate()).Select(w => (item1: w, item2: new Delegate(w))).ToList();

            foreach (var (t, d) in tuples.Where(w => w.item2.IsIsolatedDelegate))
                delegates.Add(new KeyValuePair<Type, IMemberDeclaration<DelegateDeclarationSyntax>>(t, d));

            return delegates.Select(w => WrapAsCompilationUnit(w.Key, w.Value, directive)).ToList();
        }

        private static ITypeDeclaration<BaseTypeDeclarationSyntax> CreateAbstractClass(Type t)
        {
            switch (t)
            {
                // ReSharper disable PatternAlwaysOfType

                case Type _ when t.IsInterface:
                    return new Interface(t);

                case Type _ when t.IsEnum:
                    return new Enum(t);

                case Type _ when t.IsValueType && !t.IsPrimitive && !t.IsEnum:
                    return new Struct(t);

                case Type _ when t.IsClass:
                    return new Class(t);

                default:
                    return null;

                // ReSharper restore PatternAlwaysOfType
            }
        }

        private static CompilationUnit WrapAsCompilationUnit(Type t, ITypeDeclaration<BaseTypeDeclarationSyntax> declaration, string directive)
        {
            var unit = SyntaxFactory.CompilationUnit();
            var @namespace = new Namespace(t);

            var usings = declaration.References.Where(w => !string.IsNullOrWhiteSpace(w.Namespace))
                                    .GroupBy(w => w.Namespace)
                                    .Select(w => w.First())
                                    .OrderBy(w => w.Namespace, new UsingComparer())
                                    .Select(w => new Using(w, false))
                                    .Select(w => w.DeclarationToSyntax(false));

            unit = unit.AddUsings(usings.ToArray());

            if (@namespace.HasNamespace)
            {
                var decl = @namespace.DeclarationToSyntax(true).AddMembers(declaration.DeclarationToSyntax(true));
                unit = unit.AddMembers(decl);
            }
            else
            {
                unit = unit.AddMembers(declaration.DeclarationToSyntax(true));
            }

            return new CompilationUnit(t, unit, directive);
        }

        private static CompilationUnit WrapAsCompilationUnit(Type t, IMemberDeclaration<DelegateDeclarationSyntax> declaration, string directive)
        {
            var unit = SyntaxFactory.CompilationUnit();
            var @namespace = new Namespace(t);

            if (@namespace.HasNamespace)
            {
                var decl = @namespace.DeclarationToSyntax(true).AddMembers(declaration.DeclarationToSyntax(true));
                unit = unit.AddMembers(decl);
            }
            else
            {
                unit = unit.AddMembers(declaration.DeclarationToSyntax(true));
            }

            return new CompilationUnit(t, unit, directive, "FantasmaDelegate__");
        }

        private void Flush()
        {
            // flush files
            foreach (var unit in _units)
                unit.Flush(AssetDatabase.GetAssetPath(_root));

            // flush Assembly Definitions
            foreach (var assembly in _reversed)
                WriteAssemblyDefinition(assembly);

            AssetDatabase.Refresh();
        }

        private void WriteAssemblyDefinition(Assembly assembly)
        {
            var path = Path.Combine(AssetDatabase.GetAssetPath(_root), assembly.GetName().Name, $"{assembly.GetName().Name}.asmdef");
            var references = assembly.GetReferencedAssemblies().Where(w => _assemblies.Any(v => w.FullName == v.FullName)).Select(w => w.Name).ToList();
            var constraints = new List<string>();
            if (!string.IsNullOrWhiteSpace(_directive))
                constraints.Add(_directive);

            var json = EditorJsonUtility.ToJson(new AssemblyDefinition { name = assembly.GetName().Name, references = references, defineConstraints = constraints });

            using (var sr = new StreamWriter(path))
                sr.WriteLine(json);
        }

        [Serializable]
        private class AssemblyDefinition
        {
            // ReSharper disable InconsistentNaming
            // ReSharper disable NotAccessedField.Local

            public string name;

            public List<string> references;

            public List<string> defineConstraints;

            // ReSharper restore NotAccessedField.Local
            // ReSharper restore InconsistentNaming
        }
    }
}